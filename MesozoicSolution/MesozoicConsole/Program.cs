﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicConsole
{
    class Program
    {
        static string getName(Mesozoic.Dinosaur dinosaur)
        {
            return dinosaur.name;
        }

        static string getSpecie(Mesozoic.Dinosaur dinosaur)
        {
            return dinosaur.specie;
        }

        static int getAge(Mesozoic.Dinosaur dinosaur)
        {
            return dinosaur.age;
        }

        static string setName()
        {
            Console.Write("Donner son nom : ");
            string nom = Console.ReadLine();
            return nom;
        }

        static string setSpecie()
        {
            Console.Write("Donner son espece : ");
            string specie = Console.ReadLine();
            return specie;
        }

        static int setAge()
        {
            int age;
            string age_string;
            do
            {
                Console.Write("Donner son age : ");
                age_string = Console.ReadLine();
            } while (!int.TryParse(age_string, out age));
            return age;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Donner naissance à un dinosaure : ");
            Mesozoic.Dinosaur dinosaur1 = new Mesozoic.Dinosaur("", "", 0);
            dinosaur1.name = setName();
            dinosaur1.specie = setSpecie();
            dinosaur1.age = setAge();

            Console.WriteLine(dinosaur1.hug(dinosaur1));
            Console.WriteLine(dinosaur1.SayHello());
            Console.WriteLine(dinosaur1.Roar());
            Console.ReadKey();
        }

    }
}
